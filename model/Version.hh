<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\versionControl\model
{
	use nuclio\plugin\database\orm\Model;
	
	/**
	 * @Collection version
	 * 
	 * Version data model
	 * 
	 * represent Version collection in database
	 * 
	 * @package    nuclio\plugin\versionControl\model
	 */
	class Version extends Model
	{
		/**
		 * @Id(strategy="AUTO")
		 * 
		 * @var        mixed $id 	Version ID
		 * 
		 * @access public
		 */
		public mixed $id=null;
		
		/**
		 * @mixed
		 * 
		 * @var        mixed $time 	Version time
		 * 
		 * @access public
		 */
		public mixed $time=null;
		
		/**
		 * @String
		 * 
		 * @var        string|null $action 	Version action value
		 * 
		 * @access public
		 */
		public ?string $action=null;

		/**
		 * @String
		 * 
		 * @var        string|null $collection 	Version collection
		 * 
		 * @access public
		 */
		public ?string $collection=null;
		
		/**
		 * @Map<string,mixed>
		 * 
		 * @var        Map<string,mixed>|null $old 	Version old value
		 * 
		 * @access public
		 */
		public ?Map<string,mixed> $old=null;
		
		/**
		 * @Map<string,mixed>
		 * 
		 * @var        Map<string,mixed>null $new 	Version new value
		 * 
		 * @access public
		 */
		public ?Map<string,mixed> $new=Map{};
	}
}
