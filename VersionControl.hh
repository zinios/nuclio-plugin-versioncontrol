<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\versionControl
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\application\baseController\BaseController;
	use nuclio\plugin\database\datasource\manager\Manager as DataSourceManager;
	use nuclio\plugin\database\datasource\source\Source;
	use nuclio\plugin\database\common\DBRecord;
	use nuclio\plugin\database\common\DBQuery;
	use nuclio\plugin\database\orm\ORM;
	use nuclio\plugin\user\model\User;
	use nuclio\plugin\versionControl\model\Version;

	/**
	 * A singleton class responsible for tracking version of action in the system
	 * Trace actions like insert, update and delete
	 * 
	 * @package    nuclio\plugin\versionControl
	 */
	<<singleton>>
	class VersionControl extends Plugin
	{
		/**
		 * @var        Map<string,mixed> $before 	Keeps data before change
		 * 
		 * @access private
		 */
		private Map<string,mixed> $before=Map{};

		/**
		 * @var        Map<string,mixed> $after 	Keeps data after change
		 * 
		 * @access private
		 */
		private Map<string,mixed> $after=Map{};

		/**
		 * @var        string $collection 	Keeps collection name
		 * 
		 * @access private
		 */
		private string $collection;

		/**
		 * Static method to create an instance of VersionControl class
		 * 
		 * Create an instance of VersionControl class if there is no existing one
		 * 
		 * @access public
		 * 
		 * @return     VersionControl An instance of VersionControl class
		 */
		public static function getInstance(/* HH_FIXME[4033] */ ...$args):VersionControl
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self();
		}

		/**
		 * VersionControl constructor
		 * 
		 * @access public
		 */
		public function __construct()
		{
			parent::__construct();
		}

		/**
		 * Get data before the query execution
		 * 
		 * @access public
		 * 
		 * @param      string $cllection Collection name 
		 * @param      DBRecord $record Record to be created/updated
		 * 
		 * @return      void
		 */
		public function getBeforeUpsert(string $collection, DBRecord $record):void
		{
			$this->collection=$collection;

			if(!is_null($record->get('id')))
			{
				$this->before=User::findOne(Map{'_id'=>$record->get('id')})->toMap();
			}
			else
			{
				$this->before=Map{};
			}
		}

		/**
		 * Check the data after query execution
		 * 
		 * @access public
		 * 
		 * @param      DBRecord $record Record to be created/updated
		 * 
		 * @return     void
		 */
		public function checkDiff(DBRecord $record):void
		{
			if(!is_null($record->get('_id')))
			{
				$this->after=User::findOne(Map{'_id'=>$record->get('_id')->{'$id'}})->toMap();
			}
			else
			{
				$this->after=User::findOne($record)->toMap();
			}
			$this->execute($this->before,$this->after);
		}

		/**
		 * Get the before delete
		 *
		 *@access public
		 *
		 * @param      string   $collection  Collection Name
		 * @param      DBQuery  $query       Query to get data before delete
		 * 
		 * @return     void
		 */
		public function getBeforeDelete(string $collection, DBQuery $query):void
		{
			$this->collection=$collection;
			$date=strtotime(date("Y-m-d H:i:s"));

			$cursor=User::find($query);
			for($i=0,$j=$cursor->count();$i<$j;$i++)
			{
				$version=Version::create();
				$version->setTime($date);
				$version->setAction('delete');
				$version->setCollection($this->collection);
				$version->setOld($cursor->current()->toMap());
				$version->setNew(Map{});
				$version->save();

				$cursor->next();
			}
		}

		/**
		 * Execute the version saving instruction
		 * 
		 * @access private
		 * 
		 * @param      Map<string,mixed> $before Data before change
		 * @param      Map<string,mixed> $after Data after change
		 * 
		 * @return     void
		 */
		private function execute(Map<string,mixed> $before, Map<string,mixed> $after):void
		{
			$date=strtotime(date("Y-m-d H:i:s"));

			$keyToRemove=Vector{};

			if(count($before)!=0)
			{
				if(count($after)!=0)
				{
					foreach($after as $key=>$val)
					{
						if($before->containsKey($key))
						{
							if($before->get($key)==$after->get($key))
							{
								$keyToRemove->add($key);
							}
						}
					}

					foreach($keyToRemove as $key)
					{
						$after->removeKey($key);
						$before->removeKey($key);
					}

					if(count($after)!=0)
					{
						$version=Version::create();
						$version->setTime($date);
						$version->setAction('update');
						$version->setCollection($this->collection);
						$version->setOld($before);
						$version->setNew($after);
						$version->save();
					}
				}
			}
			else
			{
				$after->removeKey('id');
				$version=Version::create();
				$version->setTime($date);
				$version->setAction('insert');
				$version->setCollection($this->collection);
				$version->setOld($before);
				$version->setNew($after);
				$version->save();
			}
		}
	}
}
